/*eslint no-console: 0, no-unused-vars: 0, dot-notation: 0, no-use-before-define: 0, no-redeclare: 0*/
"use strict";

$.import("h2s.xsjs.session.xsjslib", "session");
var SESSIONINFO = $.h2s.xsjs.session;

function insertParticipant(param) {
	var after = param.afterTableName;
	var tableName = '"H2S"."h2s.schema::h2s.Participant"';

	//Get Input New Record Values
	var pStmt = param.connection.prepareStatement('select * from "' + after + '"');
	var Participant = SESSIONINFO.recordSetToJSON(pStmt.executeQuery(), 'Details');
	pStmt.close();
	if (!Participant.Details[0]) {
		throw ('Participant is null: ' + JSON.stringify(Participant));
	}

	pStmt = param.connection.prepareStatement('select "H2S"."h2s.schema.sequence::participant".NEXTVAL from dummy');
	var rs = pStmt.executeQuery();
	var nextId = "";
	while (rs.next()) {
		nextId = rs.getString(1);
	}
	pStmt.close();

	//Insert Record into DB Table and Temp Output Table
	for (var i = 0; i < 2; i++) {
		if (i < 1) {
			pStmt = param.connection.prepareStatement("insert into " + tableName + " values(?,?,?, null)");
		} else {
			pStmt = param.connection.prepareStatement("TRUNCATE TABLE \"" + after + "\"");
			pStmt.executeUpdate();
			pStmt.close();
			pStmt = param.connection.prepareStatement("insert into \"" + after + "\" values(?,?,?, null)");
		}
		pStmt.setString(1, nextId); //id
		pStmt.setString(2, Participant.Details[0].firstName); //firstName
		pStmt.setString(3, Participant.Details[0].lastName); //lastName
		pStmt.executeUpdate();
		pStmt.close();
	}

}