# Hana Services - SAP Cloud Platform


**Some documentation references:**  
!!!CHECK VERSION NUMBERS ON PLATFORM!!!  
(documentation might differ)  

Back-end:  
* SAP HANA HDBTable Syntax Reference : https://help.sap.com/doc/527dfaaefb4e42d894fa204b04e16861/2.0.03/en-US/SAP_HANA_HDBTable_Syntax_Reference_en.pdf  
* CDS Data types - https://help.sap.com/doc/29ff91966a9f46ba85b61af337724d31/2.0.02/en-US/SAP_HANA_Core_Data_Services_CDS_Reference_en.pdf
* odata v2 : https://www.odata.org/documentation/odata-version-2-0/  
* xsjs : https://help.sap.com/doc/3de842783af24336b6305a3c0223a369/1.0.12/en-US/index.html 

Front-end/UI:  
* ODataModel - https://sapui5.hana.ondemand.com/#/api/sap.ui.model.odata.v2.ODataModel 
* JSONModel - https://sapui5.hana.ondemand.com/#/api/sap.ui.model.json.JSONModel
